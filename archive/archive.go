package archive

import (
	"io/ioutil"
	"path/filepath"
	"sync"

	"github.com/silkeh/ldapmailings/directory"
	"github.com/silkeh/ldapmailings/mail"
)

// Archive represents emails from disk.
type Archive struct {
	sync.RWMutex
	Lists     map[string]*List
	Directory *directory.Directory
	baseDir   string
}

// NewArchive creates a new (empty) archive.
// Ensures existence of the archive directory.
func NewArchive(dir string, directory *directory.Directory) (a *Archive, err error) {
	a = &Archive{
		Lists:     make(map[string]*List),
		Directory: directory,
		baseDir:   dir,
	}

	// Ensure directory exists
	err = ensureDirExists(dir)

	return
}

// LoadArchive loads an archive from disk.
func LoadArchive(dir string, directory *directory.Directory) (a *Archive, err error) {
	a, err = NewArchive(dir, directory)
	if err != nil {
		return
	}

	err = a.Load()
	return
}

// Load (re)loads the archive from disk.
func (a *Archive) Load() error {
	a.Lock()
	defer a.Unlock()

	// Get directories of mailing lists
	lists, err := ioutil.ReadDir(a.baseDir)
	if err != nil {
		return err
	}

	// Clear current index
	a.Lists = make(map[string]*List, len(lists))

	// Create and load all lists
	for _, l := range lists {
		if !l.IsDir() {
			continue
		}

		// Get corresponding group from LDAP directory,
		// or skip if it doesn't exist
		list := a.Directory.Group(l.Name())
		if list == nil {
			continue
		}

		// Load mailing list from disk
		a.Lists[list.ID], err = LoadList(list, filepath.Join(a.baseDir, l.Name()))
		if err != nil {
			return err
		}
	}

	return nil
}

// Add adds an email to the archive.
func (a *Archive) Add(msg *mail.Message) (err error) {
	a.Lock()
	defer a.Unlock()

	// Get mailing list from message
	list := msg.List

	// Ensure list exists
	l, ok := a.Lists[list.ID]
	if !ok {
		l, err = NewList(list, filepath.Join(a.baseDir, list.ID))
		if err != nil {
			return err
		}
		a.Lists[list.ID] = l
	}

	// Add to archive
	return l.Add(msg)
}

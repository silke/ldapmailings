package archive

import "time"

// Entry represents an email on disk.
type Entry struct {
	Date    time.Time
	Subject string
	From    string
	File    string
}

package archive

import (
	"fmt"
	"os"
)

// ensureDirExists ensures a directory exists,
// and is a directory
func ensureDirExists(dir string) error {
	info, err := os.Stat(dir)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(dir, 0750)
		}
	} else if !info.IsDir() {
		err = fmt.Errorf("not a directory: %s", dir)
	}
	return err
}

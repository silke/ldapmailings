package main

import (
	"io/ioutil"

	"github.com/silkeh/ldapmailings/directory"
	"github.com/silkeh/ldapmailings/ldap"
	"github.com/silkeh/ldapmailings/mail"
	"gopkg.in/yaml.v2"
)

// Config contains the application configuration.
type Config struct {
	Directory  *directory.Config `yaml:"directory"`
	LDAP       *ldap.Config      `yaml:"ldap"`
	MailServer *mail.Config      `yaml:"mail_server"`
}

// loadConfig loads configuration
func loadConfig(path string) (config *Config, err error) {
	config = new(Config)

	// Get configuration data
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return config, err
	}

	// Parse configuration
	err = yaml.Unmarshal(data, config)
	if err != nil {
		return
	}

	return
}

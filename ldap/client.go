package ldap

import (
	"crypto/tls"
	"net/url"

	"github.com/go-ldap/ldap"
)

const (
	groupFilter = "(&(objectClass=posixGroup))"
	userFilter  = "(&(objectClass=posixAccount))"
)

// Client represents an LDAP server connection.
// It is an extended version of an LDAP connection from go-ldap.
type Client struct {
	*ldap.Conn
	groupBase, userBase string
}

// NewClient creates and connects to a server using a username and password.
func NewClient(config *Config) (s *Client, err error) {
	s = &Client{
		groupBase: config.GroupBase,
		userBase:  config.UserBase,
	}

	// Connect to server
	s.Conn, err = ldap.DialURL(config.URL)
	if err != nil {
		return
	}

	// Upgrade using StartTLS if required
	if config.StartTLS {
		host, _, _ := parseURL(config.URL)
		err = s.StartTLS(&tls.Config{
			ServerName: host,
		})
		if err != nil {
			return
		}
	}

	// Login using username/password
	if config.Password != "" {
		err = s.Bind(config.Username, config.Password)
	} else {
		err = s.UnauthenticatedBind(config.Username)
	}

	return
}

// parseURL parses an LDAP url
func parseURL(addr string) (host, port string, err error) {
	var lurl *url.URL

	lurl, err = url.Parse(addr)
	if err != nil {
		return
	}

	return lurl.Hostname(), lurl.Port(), nil
}

// simpleSearch is a version of search with some sane defaults for the search request.
func (c *Client) simpleSearch(base, filter string, attrs []string) (*ldap.SearchResult, error) {
	return c.Search(ldap.NewSearchRequest(
		base,
		ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases,
		0, 0, false,
		filter,
		attrs,
		nil,
	))
}

// Users returns a list of users present on the LDAP server
func (c *Client) Users() ([]*User, error) {
	result, err := c.simpleSearch(c.userBase, userFilter, []string{"uid", "cn", "mail"})
	if err != nil {
		return nil, err
	}

	users := make([]*User, 0, len(result.Entries))
	for _, entry := range result.Entries {
		user, err := NewUser(entry)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}

	return users, nil
}

// Groups returns a list of groups present on the LDAP server
func (c *Client) Groups() ([]*Group, error) {
	result, err := c.simpleSearch(c.groupBase, groupFilter, []string{"dn", "cn", "memberuid"})
	if err != nil {
		return nil, err
	}

	groups := make([]*Group, 0, len(result.Entries))
	for _, entry := range result.Entries {
		group, err := NewGroup(entry)
		if err != nil {
			return nil, err
		}
		groups = append(groups, group)
	}

	return groups, nil
}

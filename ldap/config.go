package ldap

// Config represents the configuration for an LDAP client.
type Config struct {
	// URL, Username and Password contain the connection URL and credentials for connecting to the LDAP server.
	URL, Username, Password string

	// StartTLS enables StartTLS on the connection.
	StartTLS bool

	// GroupBase contains the base in which groups are searched.
	GroupBase string `yaml:"group_base"`

	// UserBase contains the base in which users are searched.
	UserBase string `yaml:"user_base"`
}

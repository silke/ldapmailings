package ldap

import (
	"strings"

	"github.com/go-ldap/ldap"
)

// Group represents an LDAP group.
type Group struct {
	DN, Name string
	Members  []string
}

// NewGroup creates a Group from an LDAP entry.
func NewGroup(entry *ldap.Entry) (*Group, error) {
	g := &Group{DN: entry.DN}
	for _, attr := range entry.Attributes {
		switch strings.ToLower(attr.Name) {
		case "cn":
			g.Name = attr.Values[0]
		case "memberuid":
			g.Members = attr.Values
		}
	}

	return g, nil
}

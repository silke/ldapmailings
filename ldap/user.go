package ldap

import (
	"strings"

	"github.com/go-ldap/ldap"
)

// User represents an LDAP user.
type User struct {
	DN, ID, Name string
	Email        []string
}

// NewUser creates a User from an LDAP entry.
func NewUser(entry *ldap.Entry) (*User, error) {
	u := &User{DN: entry.DN}
	for _, attr := range entry.Attributes {
		switch strings.ToLower(attr.Name) {
		case "uid":
			u.ID = attr.Values[0]
		case "cn":
			u.Name = attr.Values[0]
		case "mail":
			u.Email = attr.Values
		}
	}

	return u, nil
}

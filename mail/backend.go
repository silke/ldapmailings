package mail

import (
	"github.com/emersion/go-smtp"
)

// Backend contains the backend implementation for the mail server.
// It implements smtp.Backend.
type Backend struct {
	GetList    ListRetriever
	Directory  string
	HandleFunc *MessageHandler
}

// Login handles a login command with username and password.
func (bkd *Backend) Login(state *smtp.ConnectionState, username, password string) (smtp.Session, error) {
	return nil, smtp.ErrAuthUnsupported
}

// AnonymousLogin requires clients to authenticate using SMTP AUTH before sending emails.
func (bkd *Backend) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	return &Session{backend: bkd}, nil
}

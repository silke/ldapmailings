package mail

// Config contains the configuration of an SMTP server.
type Config struct {
	// Address is the address to listen on.
	Address string `yaml:"address"`

	// Domain is the domain of this mail server.
	Domain string `yaml:"domain"`

	// SMTPServer contains the address the SMTP server to use for sending mail.
	SMTPServer string `yaml:"smtp_server"`

	// Directory is the directory used for storing received mails.
	Directory string `yaml:"directory"`
}

// ListRetriever represents a function that can be used to retrieve a mailing list.
type ListRetriever func(to string) *List

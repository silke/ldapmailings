package mail

import (
	"fmt"
	"net/mail"
	"strings"
)

// List represents a mailing list
type List struct {
	ID, Name, Address, URL string
	Members                []*Recipient
	Hidden                 bool
}

// IDString returns the mailing list ID string for use in mail headers.
func (l *List) IDString() string {
	return fmt.Sprintf("%s <%s>",
		l.Name, strings.ReplaceAll(l.Address, "@", "."))
}

// Header returns the mailing list headers.
func (l *List) Header() mail.Header {
	return mail.Header{
		"Precedence":       []string{"list"},
		"X-Loop":           []string{fmt.Sprintf(`<%s>`, l.Address)},
		"List-Id":          []string{l.IDString()},
		"List-Archive":     []string{fmt.Sprintf(`<%s>`, l.URL)},
		"List-Owner":       []string{fmt.Sprintf(`<mailto:%s>`, l.Address)},
		"List-Help":        []string{fmt.Sprintf(`<mailto:%s?subject=Help>`, l.Address)},
		"List-Subscribe":   []string{fmt.Sprintf(`<mailto:%s?subject=Please%%20add%%20me%%20to%%20this%%20list>`, l.Address)},
		"List-Unsubscribe": []string{fmt.Sprintf(`<mailto:%s?subject=Please%%20remove%%20me%%20from%%20this%%20list>`, l.Address)},
		"List-Post":        []string{fmt.Sprintf(`<mailto:%s>`, l.Address)},
	}
}

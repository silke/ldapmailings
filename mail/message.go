package mail

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"net/mail"
	"os"
	"regexp"
	"strings"
	"time"

	"golang.org/x/crypto/blake2b"
)

// SingleMessage is an email - recipient link.
type SingleMessage struct {
	Message   *Message
	Recipient *Recipient
}

func (m *SingleMessage) Write(w io.Writer) error {
	return m.Message.Write(w, m.Recipient)
}

// Message represents a single e-mail message on a mailing list.
type Message struct {
	*mail.Message
	List   *List
	Author string
	Date   time.Time
	Body   []byte
}

// MessageFromFile opens and parses a message from a file.
func MessageFromFile(path string, list *List) (*Message, error) {
	// Create message
	m := &Message{List: list}

	// Open file
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// Read body from file
	err = m.SetBody(f)

	return m, err
}

// SingleMessages returns a set of single messages corresponding to this message.
func (m *Message) SingleMessages() (messages []*SingleMessage) {
	messages = make([]*SingleMessage, len(m.List.Members))
	for i, rcpt := range m.List.Members {
		messages[i] = &SingleMessage{Message: m, Recipient: rcpt}
	}
	return
}

// ID returns a unique identifier for this message
func (m *Message) ID() string {
	// Use blake2b for hashing
	hash, _ := blake2b.New(8, nil)

	// Hash the entire mail
	_ = m.Write(hash, nil)

	// Return hex digest
	return hex.EncodeToString(hash.Sum(nil))
}

// FileName returns the filename for this message when stored in a maildir.
func (m *Message) FileName() string {
	// Get hostname of system
	host, err := os.Hostname()
	if err != nil {
		host = "localhost"
	}

	// Write message to file
	return fmt.Sprintf("%v.%s.%s", m.Date.Unix(), m.ID(), host)
}

// SetBody sets the body of the message from an io.Reader.
// Headers are modified as needed.
func (m *Message) SetBody(r io.Reader) (err error) {
	// Parse the message
	m.Message, err = mail.ReadMessage(r)
	if err != nil {
		return err
	}

	// Store the message body
	m.Body, err = ioutil.ReadAll(m.Message.Body)

	// Set author if not yet set
	if m.Author == "" {
		m.Author = m.Message.Header.Get("From")
	}

	// Set date if not yet set
	if m.Date.IsZero() {
		m.Date, _ = mail.ParseDate(m.Message.Header.Get("Date"))
	}

	return
}

// WriteRaw writes the current message to an io.Writer.
// Nothing is modified.
func (m *Message) WriteRaw(w io.Writer, r *Recipient) error {
	// Write headers
	if err := writeHeaders(w, m.Header); err != nil {
		return err
	}

	// Write the message body
	_, err := io.Copy(w, bytes.NewReader(m.Body))

	return err
}

// Write writes the current message to an io.Writer.
// The Recipient is used to modify headers as required.
func (m *Message) Write(w io.Writer, r *Recipient) error {
	// Write headers
	if err := m.writeHeaders(w, r); err != nil {
		return err
	}

	// Write the message body
	_, err := io.Copy(w, bytes.NewReader(m.Body))

	return err
}

// writeHeaders writes the current message and list headers to an io.Writer.
// The Recipient is used to modify headers as required.
func (m *Message) writeHeaders(w io.Writer, r *Recipient) (err error) {
	// Message headers
	err = m.writeMessageHeaders(w, r)
	if err != nil {
		return
	}

	// List headers
	err = writeHeaders(w, m.List.Header())
	if err != nil {
		return
	}

	// Final newline
	_, err = w.Write([]byte("\n"))

	return
}

// writeMessageHeaders writes the current message headers to an io.Writer.
// The Recipient is used to modify headers as required.
func (m *Message) writeMessageHeaders(w io.Writer, r *Recipient) (err error) {
	// Message headers
	for k, values := range m.Header {
		var v string

		// Modify certain headers
		switch k {
		// Properly format subject
		case "Subject":
			v = m.formattedSubject()

		// Modify recipient if list is hidden
		case "To":
			if r != nil && m.List.Hidden {
				v = fmt.Sprintf("%s <%s>", r.Name, r.Address)
			} else {
				v = m.Header.Get("To")
			}

		// These are set from the mailing list config
		case "Precedence",
			"X-Loop",
			"List-Id",
			"List-Archive",
			"List-Owner",
			"List-Help",
			"List-Subscribe",
			"List-Unsubscribe",
			"List-Post":
			continue

		// While headers are technically case-insensitive,
		// Rspamd doesn't like it when the case is not 'correct'.
		case "Mime-Version":
			k = "MIME-Version"
			fallthrough

		default:
			v = strings.Join(values, "\n ")
		}

		// Write header
		if err = writeHeader(w, k, v); err != nil {
			return
		}
	}

	return
}

// writeHeaders writes a set of headers to an io.Writer.
func writeHeaders(w io.Writer, headers mail.Header) error {
	for k, values := range headers {
		if err := writeHeader(w, k, values...); err != nil {
			return err
		}
	}
	return nil
}

// writeHeader writes a header to an io.Writer.
func writeHeader(w io.Writer, key string, value ...string) error {
	_, err := w.Write([]byte(fmt.Sprintf("%s: %s\n", key, strings.Join(value, "\n "))))
	return err
}

// sanitizedSubject returns the message subject with a message tag.
// Existing message tags are removed under certain conditions.
func (m *Message) formattedSubject() string {
	subject := m.Header.Get("Subject")
	tag := `[` + m.List.Name + `]`

	// Check if subject starts with tag
	if strings.HasPrefix(subject, tag) {
		return subject
	}

	// Don't add a tag to replies or forwards for this list.
	matched, err := regexp.MatchString(`(?i)^(Re|Fwd): `+regexp.QuoteMeta(tag), subject)
	if err == nil && matched {
		return subject
	}

	return tag + ` ` + subject
}

// WriteToFile writes the message to a given file.
func (m *Message) WriteToFile(path string) error {
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0640)
	if err != nil {
		return err
	}
	defer f.Close()

	return m.WriteRaw(f, nil)
}

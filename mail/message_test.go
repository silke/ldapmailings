package mail

import (
	"net/mail"
	"strings"
	"testing"
)

var testList = &List{
	ID:      "test",
	Name:    "Test",
	Address: "test@example.com",
	URL:     "example.com/lists/test",
	Members: []*Recipient{
		{Name: "Alice", Address: "alice@example.com"},
		{Name: "Bob", Address: "bob@example.com"},
	},
	Hidden: true,
}

// Tests for headers
var headerTests = []struct{ In, Out string }{
	{"from: alice@example.com", "From: alice@example.com"},
	{"to: <test@example.com>", "To: Alice <alice@example.com>"},

	// Other headers
	{"mime-version: 1.0", "MIME-Version: 1.0"},

	// Mailing list headers that should be stripped
	{"Precedence: ignored", ""},
	{"X-Loop: ignored", ""},
	{"List-Id: ignored", ""},
	{"List-Archive: ignored", ""},
	{"List-Owner: ignored", ""},
	{"List-Help: ignored", ""},
	{"List-Subscribe: ignored", ""},
	{"List-Unsubscribe: ignored", ""},
	{"List-Post: ignored", ""},

	// Subject tests
	{"subject: test message", "Subject: [Test] test message"},
	{"subject: Subject", "Subject: [Test] Subject"},
	{"subject: [Test] Subject", "Subject: [Test] Subject"},
	{"subject: Re: [Test] Subject", "Subject: Re: [Test] Subject"},
	{"subject: RE: [Test] Subject", "Subject: RE: [Test] Subject"},
	{"subject: Fwd: [Test] Subject", "Subject: Fwd: [Test] Subject"},
	{"subject: FWD: [Test] Subject", "Subject: FWD: [Test] Subject"},
	{"subject: Re: [Other] Subject", "Subject: [Test] Re: [Other] Subject"},
}

func TestMessage_writeMessageHeaders(t *testing.T) {
	// Test message
	msg := &Message{
		Message: new(mail.Message),
		List:    testList,
	}

	// Output buffer
	out := new(strings.Builder)

	// Test all headers
	for _, test := range headerTests {
		out.Reset()

		var err error
		msg.Message, err = mail.ReadMessage(strings.NewReader(test.In + "\r\n\r\n"))
		if err != nil {
			t.Errorf("Error parsing header %q: %s", test.In, err)
			t.FailNow()
		}

		err = msg.writeMessageHeaders(out, testList.Members[0])
		if err != nil {
			t.Errorf("Error serializing header %q: %s", test.In, err)
			t.FailNow()
		}

		outStr := strings.TrimSpace(out.String())
		if outStr != test.Out {
			t.Errorf("Incorrect serialization for %q: expected %q, got %q",
				test.In, test.Out, outStr)
		}
	}
}

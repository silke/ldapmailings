package mail

import "fmt"

// Recipient contains an email recipient
type Recipient struct {
	Name, Address string
}

// String returns a string version of the recipient.
// It can be used in email headers.
func (r *Recipient) String() string {
	return fmt.Sprintf("%s <%s>", r.Name, r.Address)
}

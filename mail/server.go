package mail

import (
	"errors"
	"time"

	"github.com/emersion/go-smtp"
)

const (
	// OutCapacity is the capacity of the output queue.
	OutCapacity = 250

	// OutCapacityMargin is the additional margin to keep in the output queue.
	// This margin avoids the output queue becoming blocked
	// when multiple messages arrive simultaneously.
	OutCapacityMargin = 50
)

// MessageHandler is a function that handles a message.
type MessageHandler func(*Message) error

// Server is an SMTP server.
type Server struct {
	*smtp.Server
	Client     *Client
	OutQueue   chan *SingleMessage
	HandleFunc MessageHandler
}

// NewServer creates a new SMTP server.
func NewServer(config *Config, listFunc ListRetriever) (*Server, error) {
	// Create SMTP client for outgoing mails
	client, err := NewClient(config.SMTPServer)
	if err != nil {
		return nil, err
	}

	// Create server
	s := &Server{
		Client:   client,
		OutQueue: make(chan *SingleMessage, OutCapacity),
	}

	// Create basic SMTP server
	s.Server = smtp.NewServer(&Backend{HandleFunc: &s.HandleFunc, Directory: config.Directory, GetList: listFunc})
	s.Addr = config.Address
	s.Domain = config.Domain
	s.ReadTimeout = 30 * time.Second
	s.WriteTimeout = 30 * time.Second
	s.MaxMessageBytes = 104857600
	s.MaxRecipients = 10
	s.AuthDisabled = true

	// Default handler
	s.HandleFunc = s.HandleMessage

	return s, nil
}

// ListenAndServe listens on the network address s.Addr and serves incoming connections.
// Incoming messages are automatically added to OutQueue.
func (s *Server) ListenAndServe() error {
	defer close(s.OutQueue)

	return s.Server.ListenAndServe()
}

// HandleMessage handles an incoming message from the queue or some external component.
func (s *Server) HandleMessage(in *Message) error {
	messages := in.SingleMessages()

	// Check for sufficient capacity
	if len(s.OutQueue)+len(messages)+OutCapacityMargin > OutCapacity {
		return errors.New("server capacity exceeded")
	}

	// Add mails to output queue
	for _, out := range messages {
		s.OutQueue <- out
	}
	return nil
}

// OutWorker takes messages from OutQueue and attempts to send them.
func (s *Server) OutWorker() error {
	return s.Client.Worker(s.OutQueue)
}

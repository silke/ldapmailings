package main

import (
	"flag"
	"log"
	"time"

	"github.com/silkeh/ldapmailings/archive"
	"github.com/silkeh/ldapmailings/directory"
	"github.com/silkeh/ldapmailings/mail"
)

const (
	cacheDuration = 300 * time.Second
	retryDuration = 10 * time.Second
)

// cacheHandler handles cache refreshes
func cacheHandler(d *directory.Directory) {
	ticker := time.NewTicker(cacheDuration)
	defer ticker.Stop()

	for range ticker.C {
		log.Printf("Refreshing directory cache...")
		err := d.Refresh()
		if err != nil {
			log.Print("Error refreshing directory cache:", err)
		} else {
			log.Print("Successfully refreshed cache!")
		}
	}
}

// outHandler handles outgoing email
func outHandler(s *mail.Server) {
	for {
		err := s.OutWorker()

		// Return on clean exit
		if err == nil {
			return
		}

		// Some error has occurred, retry in a bit
		log.Printf("Error sending mail: %s", err)
		time.Sleep(retryDuration)
	}
}

func main() {
	var configFile string

	flag.StringVar(&configFile, "config", "config.yaml", "Configuration file")
	flag.Parse()

	// Load configuration
	config, err := loadConfig(configFile)
	if err != nil {
		log.Fatalf("Error loading config file %s: %s", configFile, err)
	}

	// Create list directory
	d, err := directory.New(config.Directory, config.LDAP)
	if err != nil {
		log.Fatalf("Error connecting to LDAP: %s", err)
	}
	defer d.Close()

	// Load on-disk archive
	log.Printf("Loading mails from archive, this may take some time...")
	arch, err := archive.LoadArchive(config.MailServer.Directory, d)
	if err != nil {
		log.Fatalf("Error reading mail archive: %s", err)
	}

	// Ensure the cache is refreshed regularly
	go cacheHandler(d)

	// Create mail server
	ms, err := mail.NewServer(config.MailServer, d.List)
	if err != nil {
		log.Fatalf("Error creating SMTP server: %s", err)
	}

	// Setup archive and mail handlers
	ms.HandleFunc = func(message *mail.Message) error {
		if err := ms.HandleMessage(message); err != nil {
			return err
		}
		return arch.Add(message)
	}

	// Ensure mail goes out
	go outHandler(ms)

	// Start the mail server
	log.Printf("Listening on %s", config.MailServer.Address)
	log.Fatal(ms.ListenAndServe())
}
